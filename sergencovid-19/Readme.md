# PEP installation

The nginx ansible role is <https://gitea-s2i2s.isti.cnr.it/ISTI-ansible-roles/ansible-role-nginx>

## Installation paths of the nginx and PEP components 

### Virtualhost

The virtualhost is installed under `/etc/nginx/sites-available/<FQDN>.conf`, and then symlinked under `/etc/nginx/sites-enabled/<FQDN>.conf`.

### PEP files

The PEP files are installed under `/etc/nginx/conf.d`

