# Variables that must be defined

```yaml
docker_registry_host: 'docker.io'
docker_registry_username: 'd4science'
docker_regitry_password: 'use a vault file'
docker_source_image: "{{ docker_registry_host }}/nginx:alpine"
docker_target_image: "d4science/nginx-{{ docker_service_server_name }}"
docker_image_maintainer: "D4Science <d4science@d4science.org>"
docker_stack_name: d4science-stack
docker_service_server_name: d4science-service
docker_service_host: "external-service-fqdn"
docker_service_endpoint: "https://{{ docker_service_host }}/"
docker_service_port: 8080
d4s_api_endpoint: "https://api.d4science.org/rest/2/people/profile"
d4s_accounting_endpoint: "https://accounting-service.d4science.org/accounting-service/record"
d4s_authorized_scopes: "string"
```
